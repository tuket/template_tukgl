#include "tukgl/util/auxiliar.hpp"
#include <SDL.h>
#include <glad/glad.h>
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/constants.hpp>
#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

#include "tukgl/render/renderer.hpp"
#include "tukgl/scene/scene.hpp"

using namespace std;
using namespace glm;

static SDL_Window* window;
static bool run;

// Constants
const char* WINDOW_TITLE = "PGATR 3";
const unsigned SCREEN_WIDTH = 800;
const unsigned SCREEN_HEIGHT = 600;
const float NEAR_PLANE_DIST = 0.1f;
const float FAR_PLANE_DIST = 500.f;
const float FOV_Y = 60;
const float CAMERA_SPEED = 0.1f;

// wasd keys status
bool wPressed = false, aPressed = false, sPressed = false, dPressed = false;
// mouse status
bool mousePressed = false;
int prevMouseX, prevMouseY;

float globTime = 0;

vec3 cameraPos;
float cameraHeading = 0, cameraPitch = 0;

Renderer* renderer;
Scene* scene;
SceneNode* cameraNode;
SceneNode* teapotNode;
SceneNode* grassNode;
SceneNode* groundNode;

void renderFunc();
void resizeFunc(int width, int height);
void idleFunc(float dt);
void handleEvent(const SDL_Event& event);
void updateKeyboardState();
void mouseFunc(int state, int x, int y);
void mouseMotionFunc(int x, int y);

//Funciones de inicialización y destrucción
void initContext(int argc, char** argv);
void initOGL();
void initScene();


int main(int argc, char** argv)
{

	//std::locale::global(std::locale("spanish"));// acentos ;)

	initContext(argc, argv);
	initOGL();
	initScene();
	resizeFunc(SCREEN_WIDTH, SCREEN_HEIGHT);

	static float prevTime = (float)SDL_GetTicks();
	static float curTime = (float)SDL_GetTicks();
	run = true;
	while (run)
	{

		// compute delta time
		curTime = (float)SDL_GetTicks();
		float dt = (curTime - prevTime) / 1000.f;
		prevTime = curTime;

		SDL_Event event;
		// handle events
		if (SDL_PollEvent(&event)) {
			handleEvent(event);
		}
		updateKeyboardState();

		idleFunc(dt);

		renderFunc();

		/*
		// limit FPS
		unsigned int endTicks = SDL_GetTicks();
		int sleepTicks = 1000 / LIMIT_FPS - (endTicks - beginTicks);
		if (sleepTicks > 0)
		SDL_Delay(sleepTicks);*/
	}

	return 0;
}

//////////////////////////////////////////
// Funciones auxiliares 
void initContext(int argc, char** argv)
{

    SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	// don't allow deprecated GL function
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
    
    int err = SDL_Init(SDL_INIT_EVERYTHING);
    if(err < 0)
    {
        printf("Error initilizing SDL: %s\n", SDL_GetError());
    }

	// make the window
	window =
		SDL_CreateWindow(
			WINDOW_TITLE,
			100, 100,
			SCREEN_WIDTH, SCREEN_HEIGHT,
			SDL_WINDOW_OPENGL
		);

	SDL_GLContext context = SDL_GL_CreateContext(window);
    if(context == 0)
    {
        printf("Failed to create OpenGL context\n");
    }

	if (!gladLoadGLLoader(((GLADloadproc)SDL_GL_GetProcAddress)))
	{
		printf("gladLoadGL failed\n");
	}

	const GLubyte *oglVersion = glGetString(GL_VERSION);
	std::cout << "This system supports OpenGL Version: " << oglVersion << std::endl;
}

void initOGL()
{
	glEnable(GL_DEPTH_TEST);
	glClearColor(0.15f, 0.15f, 0.15f, 1.0f);

	glFrontFace(GL_CCW);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glEnable(GL_CULL_FACE);

	renderer = new Renderer();
	resizeFunc(SCREEN_WIDTH, SCREEN_HEIGHT);

}

void initScene()
{

	cameraPos = vec3(0, 0, 4);
	cameraHeading = 0;
	cameraPitch = 0;

	scene = new Scene();

	cameraNode = new SceneNode();
	GOCamera* goCamera = new GOCamera(cameraNode);
	cameraNode->setPosition(vec3(0, 5, 20));
	//cameraNode->setRotation()

	ShaderPool& shaderPool = ShaderPool::getInstance();
	ShaderId grassShaderId = shaderPool.registerPrePassShader(
		"test_gem_shader",
		"shaders/pre_grass.vert",
		"shaders/pre_grass.geom",
		"shaders/pre_grass.frag");
	Material grassMat;
	grassMat.setShadingModelId(ShadingModelId::UNLIT);
	grassMat.setShaderID(grassShaderId);
	grassMat.setFaceCulling(false);
	grassMat.setColorTexture("img/grass.png");

	grassNode = new SceneNode();
	GOMesh* planeMesh = GOMesh::createPlane(20, 20, 10, 10, 3, 3);
	grassNode->setGameObject(planeMesh);
	planeMesh->setMaterial(grassMat);
	grassNode->setPosition(vec3(0, 0, 0));

	groundNode = new SceneNode();
	GOMesh* groundMesh = new GOMesh();
	groundMesh->setMeshFromId(planeMesh->getMeshId());
	groundNode->setGameObject(groundMesh);
	Material groundMat;
	groundMat.loadMaterialFromFile("mats/mat_unlit.xml");
	groundMat.setColorTexture("img/ground.jpg");
	groundMesh->setMaterial(groundMat);
	groundNode->setPosition(vec3(0, 0, 0));

	SceneNode* root = new SceneNode();
	root->attachChild(grassNode);
	root->attachChild(groundNode);
	root->attachChild(cameraNode);

	scene->setRoot(root);
	scene->setActiveCamera(goCamera);

}


void renderFunc()
{

	renderer->render(scene);

	SDL_GL_SwapWindow(window);
}

void resizeFunc(int width, int height)
{
	renderer->resize(width, height);
}

void idleFunc(float dt)
{

	// compute fps and set as window title
	const float RECOMP_TIME = 1.f;
	static unsigned frames = 0;
	static float acumTime = 0.f;
	acumTime += dt;
	frames++;
	if (acumTime > RECOMP_TIME)
	{
		float fps = frames / acumTime;
		const unsigned TITLE_MAX_SIZE = 32;
		char title[TITLE_MAX_SIZE];
		snprintf(title, TITLE_MAX_SIZE, "%.2f", fps);
		SDL_SetWindowTitle(window, title);
		acumTime = 0.f;
		frames = 0;
	}

	// camera control
	glm::vec3 translate(0);
	bool bMove = wPressed || sPressed || aPressed || dPressed;

	if (bMove)
	{
		if (wPressed) translate.z = -1;
		else if (sPressed) translate.z = +1;

		if (aPressed) translate.x = -1;
		else if (dPressed) translate.x = +1;

		translate = glm::normalize(translate);
	}

	translate = CAMERA_SPEED * translate;
	cameraNode->moveSelf(translate);

	// teapot
	GOMesh* grassMesh = (GOMesh*)grassNode->getGameObject();
	grassMesh->getMaterial().setUnifValue("time", globTime);

	globTime += dt;
}

void handleEvent(const SDL_Event& event)
{

	switch (event.type) {
		/* close button clicked */
	case SDL_QUIT:
		run = false;
		break;

		/* handle the mouse */
	case SDL_MOUSEBUTTONDOWN:
	{
		if (event.button.button == SDL_BUTTON_LEFT)
		{
			int x = event.button.x;
			int y = event.button.y;
			mouseFunc(0, x, y);
		}
		break;
	}
	case SDL_MOUSEBUTTONUP:
	{
		if (event.button.button == SDL_BUTTON_LEFT)
		{
			int x = event.button.x;
			int y = event.button.y;
			mouseFunc(1, x, y);
		}
		break;
	}
	case SDL_MOUSEMOTION:
	{
		int x = event.motion.x;
		int y = event.motion.y;
		mouseMotionFunc(x, y);
		break;
	}

	/* window events (resize)*/
	case SDL_WINDOWEVENT:
	{

		break;
	}
	}

}

void updateKeyboardState()
{
	const Uint8* keyboard = SDL_GetKeyboardState(NULL);
	wPressed = keyboard[SDL_SCANCODE_W];
	aPressed = keyboard[SDL_SCANCODE_A];
	sPressed = keyboard[SDL_SCANCODE_S];
	dPressed = keyboard[SDL_SCANCODE_D];
}

void mouseFunc(int state, int x, int y)
{
	if (state == 0)	 // pressed
	{
		mousePressed = true;
		prevMouseX = x;
		prevMouseY = y;
	}
	else             // released
	{
		mousePressed = false;
	}
}

void mouseMotionFunc(int x, int y)
{
	const float PITCH_LIMIT = 0.4f*PI;
	const float SENSITIVITY = 5.0f;
	if (mousePressed)
	{
		int dx = x - prevMouseX;
		int dy = y - prevMouseY;

		float fdx = dx * SENSITIVITY / renderer->getScreenHeight();
		float fdy = dy * SENSITIVITY / renderer->getScreenHeight();

		cameraHeading -= fdx;
		normalizeAngle2PI(cameraHeading);
		cameraPitch -= fdy;
		cameraPitch = clamp(cameraPitch, -PITCH_LIMIT, PITCH_LIMIT);

		prevMouseX = x;
		prevMouseY = y;

		cameraNode->setRotation(glm::quat(glm::vec3(cameraPitch, cameraHeading, 0.f)));
	}
}
